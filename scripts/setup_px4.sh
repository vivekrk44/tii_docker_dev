HOME=/home/tii_dev

pip3 install --user empy packaging jinja2 pyyaml toml

git clone https://github.com/PX4/Firmware
cd Firmware
git checkout v1.9.2
DONT_RUN=1 make px4_sitl gazebo_standard_vtol

cd $HOME
mkdir -p catkin_ws/src
cd catkin_ws/src 
catkin_init_workspace
cd ..
catkin build

source ~/catkin_ws/devel/setup.bash
source /home/tii_dev/Firmware/Tools/setup_gazebo.bash /home/tii_dev/Firmware /home/tii_dev/Firmware/build/px4_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/tii_dev/Firmware
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/home/tii_dev/Firmware/Tools/sitl_gazebo


